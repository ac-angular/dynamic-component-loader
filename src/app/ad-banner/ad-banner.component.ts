import { Component, OnInit, Input, ViewChild, OnDestroy, ComponentFactoryResolver } from '@angular/core';
import { AdItem } from '../ad-item';
import { AdHostDirective } from '../ad-host.directive';
import { Ad } from '../ad';

@Component({
  selector: 'app-ad-banner',
  templateUrl: './ad-banner.component.html',
  styleUrls: ['./ad-banner.component.css']
})
export class AdBannerComponent implements OnInit, OnDestroy {

  @Input() ads: AdItem[];
  currentAdIndex = -1;
  @ViewChild(AdHostDirective, {static: true}) appAdHost: AdHostDirective;
  interval: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }
  ngOnInit() {

    this.loadComponent();
    this.getAds();
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  getAds() {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 3000);
  }

  loadComponent() {
    this.currentAdIndex = (this.currentAdIndex + 1) % this.ads.length;
    const adItem = this.ads[this.currentAdIndex];

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(adItem.component);
    const viewContainierRef = this.appAdHost.viewContainerRef;
    viewContainierRef.clear();

    const componentRef = viewContainierRef.createComponent(componentFactory);
    (componentRef.instance as Ad).data = adItem.data;
  }

}
