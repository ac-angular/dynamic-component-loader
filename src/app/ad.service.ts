import { Injectable } from '@angular/core';
import { AdItem } from './ad-item';
import { AdVideoComponent } from './ad-video/ad-video.component';
import { AdImageComponent } from './ad-image/ad-image.component';

@Injectable({
  providedIn: 'root'
})
export class AdService {

  DATA = [
    {
      component: 'AdImageComponent',
      data: {
        path: 'https://image.shutterstock.com/image-photo/boy-wings-sunset-imagines-himself-600w-695361658.jpg',
        caption: 'boy'
      }
    },
    {
      component: 'AdVideoComponent',
      data: {
        url: './assets/gael-1.mp4'
      }
    },
    {
      component: 'AdImageComponent',
      data: {
        path: 'https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-600w-789676552.jpg',
        caption: 'dandelion'
      }
    },
    {
      component: 'AdVideoComponent',
      data: {
        url: './assets/gael-2.mp4'
      }
    },
    {
      component: 'AdImageComponent',
      data: {
        path: 'https://image.shutterstock.com/image-photo/boy-wings-sunset-imagines-himself-600w-695361658.jpg',
        caption: 'boy'
      }
    }
  ];
  constructor() { }

  getAds() {
    const ads = [];
    this.DATA.forEach(element => {
      switch (element.component) {
        case 'AdVideoComponent':
          ads.push(new AdItem(AdVideoComponent, {url: element.data.url}));
          break;
          case 'AdImageComponent':
            ads.push(new AdItem(AdImageComponent, {path: element.data.path, caption: element.data.caption}));
            break;
        default:
          break;
      }
    });
    return ads;
  }
}
