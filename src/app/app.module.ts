import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AdHostDirective } from './ad-host.directive';
import { AdBannerComponent } from './ad-banner/ad-banner.component';
import { AdVideoComponent } from './ad-video/ad-video.component';
import { AdImageComponent } from './ad-image/ad-image.component';

@NgModule({
  declarations: [
    AppComponent,
    AdHostDirective,
    AdBannerComponent,
    AdVideoComponent,
    AdImageComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [AdVideoComponent, AdImageComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
